package com.jktech.fingerprintlibrary;

/**
 * Created by sukeshini on 08-02-2017.
 */

public interface ConnectionCallbacks {

    public void authenticationSuccess();
    public void authenticationFailure();
}
